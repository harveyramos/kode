var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    zip = require('gulp-zip'),
    uglify = require('gulp-uglify'),
    jshint = require('gulp-jshint'),
    minifycss = require('gulp-minify-css'),
    autoprefix = require('gulp-autoprefixer'),
    notify = require('gulp-notify'),
    rename = require('gulp-rename'),
    clean = require('gulp-clean'),
    util = require('gulp-util'),
    filesize = require('gulp-filesize'),
    bower = require('gulp-bower'),
    pkg  = require('./package.json');
    
var config = {
    sassPath: './sass',
    bowerDir: './bower_components',
    cssPath: './assets/css',
    jsPath: './assets/js'
};

var zipDirs = [
    '*.*',
    'assets/**',
    'inc/**',
    'woocommerce/**',
    '!bower.json',
    '!Gulpfile.js',
    '!package.json',
    '!style.scss'
];

gulp.task('icons', function() {
    return gulp.src(config.bowerDir + '/fontawesome/fonts/**.*')
        .pipe(gulp.dest('./assets/fonts'));
});

gulp.task('styles', function() {
    return sass(
            './style.scss',
            {
                style: 'expanded',
                sourcemap: true,
                loadPath: [
                    './sass',
                    './inc/woocommerce/sass',
                    config.bowerDir + '/fontawesome/scss',
                    config.bowerDir + '/bourbon/app/assets/stylesheets',
                    config.bowerDir + '/susy/sass',
                ],
                quiet: true
            })
        .on("error", notify.onError(function (error) {
            return "Error: " + error.message;
        }))
        .pipe(autoprefix('last 2 version'))
        .pipe(rename("main.css"))
        .pipe(sourcemaps.write('./', {
            includeContent: false,
            sourceRoot: '../../'
        }))
        .pipe(gulp.dest('./assets/css'));
});

gulp.task('cssmin', function() {
    return gulp.src(config.cssPath + '/*.css')
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(minifycss())
        .pipe(gulp.dest(config.cssPath));
});

// Runs JSHint on scripts
gulp.task('scripts', function() {
    return gulp.src(config.jsPath + '/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'))
        // .pipe(jshint.reporter('fail'))
});

/**
 * Creates a zip file to distribute the stage extension
 *
 * base option of gulp.src uses '../' to go up one level so the packaged zip has the folder name inside the zip
 * per WordPress theme zip requirements.
 */
gulp.task('zip', function() {
    return gulp.src(zipDirs, { base: "../" })
        .pipe(zip(pkg.name + '-' + pkg.version + '.zip'))
        .pipe(gulp.dest('./build'));

});

// Rerun the task when a file changes
gulp.task('watch', function() {
    gulp.watch(config.sassPath + '/**/*.scss', ['css']);
});

gulp.task('deploy', ['build', 'zip']);

gulp.task('build', ['icons', 'styles', 'cssmin']);

gulp.task('default', ['styles']);