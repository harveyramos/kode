<?php
/**
 * storefront setup functions
 *
 * @package kode
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 1170; /* pixels */
}

/**
 * Assign the Storefront version to a var
 */
$theme        = wp_get_theme();
$kode_version = $theme['Version'];
