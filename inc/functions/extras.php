<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * @package kode
 */

/**
 * Scripts and stylesheets
 *
 * Enqueue stylesheets in the following order:
 * 1. /theme/assets/css/main.css
 *
 * Enqueue scripts in the following order:
 * 1. jquery-1.11.1.min.js via Google CDN
 * 2. /theme/assets/js/vendor/modernizr.min.js
 * 3. /theme/assets/js/scripts.js (in footer)
 *
 * Google Analytics is loaded after enqueued scripts if:
 * - An ID has been defined in config.php
 * - You're not logged in as an administrator
 */
function littlerabbit_scripts() {
	global $kode_version;

	/**
	* The build task in Grunt renames production assets with a hash
	* Read the asset names from assets-manifest.json
	*/
	if (WP_ENV === 'development') {
		$assets = array(
			'fonts'     => '//fonts.googleapis.com/css?family=Muli|Telex|Oswald',
			'css'       => '/assets/css/main.css',
			'js'        => '/assets/js/scripts.js',
			'jquery'    => '//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.js'
		);
	} else {
		// $get_assets = file_get_contents(get_stylesheet_directory_uri() . '/assets/manifest.json');
		// $assets     = json_decode($get_assets, true);
		$assets     = array(
			'fonts'     => '//fonts.googleapis.com/css?family=Muli|Telex|Oswald',
			'css'       => '/assets/css/main.css?' . $kode_version,
			'js'        => '/assets/js/scripts.js?' . $kode_version,
			'jquery'    => '//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js',
		);
	}
	
	wp_enqueue_style('kode_fonts', $assets['fonts']);  // load up our own fonts
	wp_enqueue_style('kode_css', get_stylesheet_directory_uri() . $assets['css'], false, null);

	/**
	* jQuery is loaded using the same method from HTML5 Boilerplate:
	* Grab Google CDN's latest jQuery with a protocol relative URL; fallback to local if offline
	* It's kept in the header instead of footer to avoid conflicts with plugins.
	*/
	if ( !is_admin() ) {
		wp_deregister_script('jquery');
		wp_register_script('jquery', $assets['jquery'], array(), null, false);
	}

	if (is_single() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}
	
	wp_enqueue_script('jquery');
	wp_enqueue_script('kode_js', get_stylesheet_directory_uri() . $assets['js'], array(), null, true);
	
}