<?php
/**
 * storefront hooks
 *
 * @package kode
 */

/**
 * General
 * @see  storefront_setup()
 * @see  storefront_widgets_init()
 * @see  storefront_scripts()
 * @see  storefront_header_widget_region()
 * @see  storefront_get_sidebar()
 */
add_action( 'wp_enqueue_scripts',			'littlerabbit_scripts',			10 );
add_action( 'storefront_before_content',	'kode_header_widget_region',	10 );

/**
 * Header
 * @see  storefront_site_branding()
 * @see  storefront_primary_navigation()
 * @see  storefront_cart_navigation()
 */
add_action( 'storefront_header', 'kode_site_branding',			20 );
add_action( 'storefront_header', 'kode_primary_navigation',		50 );
add_action( 'storefront_header', 'kode_cart_navigation',	    60 );

/**
 * Homepage
 * @see  storefront_homepage_content()
 * @see  storefront_product_categories()
 * @see  storefront_recent_products()
 * @see  storefront_featured_products()
 * @see  storefront_popular_products()
 * @see  storefront_on_sale_products()
 */
add_action( 'homepage', 'kode_homepage_content',	10 );
add_action( 'homepage', 'kode_product_categories',	20 );
add_action( 'homepage', 'kode_recent_products',		30 );
add_action( 'homepage', 'kode_featured_products',	40 );
// add_action( 'homepage', 'kode_popular_products',	50 );
add_action( 'homepage', 'kode_on_sale_products',	60 );

/**
 * Product Loop Items
 *
 * @see woocommerce_template_loop_add_to_cart()
 * @see woocommerce_template_loop_product_thumbnail()
 * @see woocommerce_template_loop_price()
 * @see woocommerce_template_loop_rating()
 */
add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_price', 20 );

/**
 * Footer
 * @see  storefront_footer_widgets()
 * @see  storefront_credit()
 */
add_action( 'storefront_footer', 'kode_credit',			20 );
