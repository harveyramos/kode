<?php
/**
 * Template functions used for the site footer.
 *
 * @package kode
 */

/**
 * Display the theme credit
 * @since  1.0.0
 * @return void
 */
function kode_credit() {
	?>
	<div class="site-info">
		&copy; <?php echo date( 'Y' ); ?> <span class="kode">KODE</span>.blue All rights reserved. Designed by <a href="http://littlerabbitstudios.com" rel="designer">Little Rabbit Studios</a>
	</div><!-- .site-info -->
	<?php
}
