<?php
/**
 * Template functions used for the site header.
 *
 * @package kode
 */

/**
 * Display header widget region
 * @since  1.0.0
 */
function kode_header_widget_region() {
	?>
	<div class="header-widget-region">
		<div class="col-full">
			<?php dynamic_sidebar( 'header-1' ); ?>
		</div>
	</div>
	<?php
}

/**
 * Display Site Branding
 * @since  1.0.0
 * @return void
 */
function kode_site_branding() {
	?>
		<div class="site-branding">
			<h1 class="site-title"><span class="kode">KODE</span>.blue</h1>
			<p class="site-description"><?php bloginfo( 'description' ); ?></p>
		</div>
	<?php
}

/**
 * Display Primary Navigation
 * @since  1.0.0
 * @return void
 */
function kode_primary_navigation() {
	?>
	<nav id="site-navigation" class="main-navigation" role="navigation">
	<button class="menu-toggle"><?php echo esc_attr( apply_filters( 'storefront_menu_toggle_text', __( 'Primary Menu', 'storefront' ) ) ); ?></button>
		<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
	</nav><!-- #site-navigation -->
	<?php
}

/**
 * Display Secondary Navigation
 * @since  1.0.0
 * @return void
 */
function kode_secondary_navigation() {
	if ( is_cart() ) {
		$class = 'current-menu-item';
	} else {
		$class = '';
	}
	?>
	<nav class="main-navigation" role="navigation">
		<ul>
			
		</ul>
	</nav><!-- #site-navigation -->
	<?php
}

/**
 * Display Primary Navigation
 * @since  1.0.0
 * @return void
 */
function kode_cart_navigation() {
	if ( is_cart() ) {
		$class = 'class="current-menu-item"';
	} else {
		$class = '';
	}
	?>
	<nav class="site-header-cart" role="navigation">
		<button class="menu-toggle"><?php echo esc_attr( apply_filters( 'storefront_menu_toggle_text', __( 'Account Menu', 'storefront' ) ) ); ?></button>
		<div class="menu-top-container">
			<ul class="menu">
				<li>
					<?php if ( is_user_logged_in() ) : ?>
					<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account','storefront'); ?>"><i class="fa fa-user"></i> <?php _e('My Account','storefront'); ?></a>
					<?php else : ?>
					<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Login / Register','storefront'); ?>"><?php _e('Login / Register','storefront'); ?></a>
					<?php endif; ?>
				</li>
				<li <?php echo $class; ?> >
					<a href="<?php echo esc_url( WC()->cart->get_cart_url() ); ?>" title="<?php _e( 'View your shopping cart', 'storefront' ); ?>">Cart (<span class="count"><?php echo wp_kses_data( WC()->cart->get_cart_contents_count() );?></span>)</a>
				</li>
			</ul>
		</div>
	</nav><!-- #site-navigation -->
	<?php
}