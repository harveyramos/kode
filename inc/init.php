<?php
/**
 * storefront engine room
 *
 * @package kode
 */

/**
 * Setup.
 * Enqueue styles, register widget regions, etc.
 */
require get_stylesheet_directory() . '/inc/functions/setup.php';

/**
 * Structure.
 * Template functions used throughout the theme.
 */
require get_stylesheet_directory() . '/inc/structure/hooks.php';
// require get_stylesheet_directory() . '/inc/structure/post.php';
// require get_stylesheet_directory() . '/inc/structure/page.php';
require get_stylesheet_directory() . '/inc/structure/header.php';
require get_stylesheet_directory() . '/inc/structure/footer.php';
// require get_stylesheet_directory() . '/inc/structure/comments.php';
require get_stylesheet_directory() . '/inc/structure/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_stylesheet_directory() . '/inc/functions/extras.php';

/*
 * Add custom update function
 */
//Initialize the update checker.
require get_stylesheet_directory() . '/inc/functions/theme-update.php';
$update_checker = new ThemeUpdateChecker(
	'kode',
	'http://littlerabbitstudios.com/api/update/kode/version'
);
