<?php
/**
 * storefront engine room
 * Child Theme functions.
 *
 * @package kode
 */

/**
 * Initialize all the things.
 */
require get_stylesheet_directory() . '/inc/init.php';

/**
 * Remove duplicate functions from main theme
 */
function kode_remove_parent_features() {
    remove_action( 'wp_enqueue_scripts',        'storefront_scripts',               10 );
    remove_action( 'wp_enqueue_scripts', 		'storefront_add_customizer_css',    130 );
    remove_action( 'wp_enqueue_scripts', 		'storefront_woocommerce_scripts',	20 );
    remove_action( 'storefront_header',         'storefront_site_branding',			20 );
    remove_action( 'storefront_header',         'storefront_secondary_navigation',	30 );
    remove_action( 'storefront_header',         'storefront_primary_navigation',	50 );
    remove_action( 'storefront_header',         'storefront_product_search',		40 );
    remove_action( 'storefront_header',         'storefront_header_cart',			60 );
    remove_action( 'storefront_before_content',	'storefront_header_widget_region',	10 );
    remove_action( 'storefront_footer',         'storefront_credit',			    20 );
    remove_action( 'homepage',                  'storefront_homepage_content',		10 );
    remove_action( 'homepage',                  'storefront_product_categories',	20 );
    remove_action( 'homepage',                  'storefront_recent_products',		30 );
    remove_action( 'homepage',                  'storefront_featured_products',		40 );
    remove_action( 'homepage',                  'storefront_popular_products',		50 );
    remove_action( 'homepage',                  'storefront_on_sale_products',		60 );
    remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
}
add_action( 'after_setup_theme', 'kode_remove_parent_features', 10 );